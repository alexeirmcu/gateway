insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000001','Gateway # 1','192.168.101.23');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000002','Gateway # 2','192.168.101.24');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000003','Gateway # 3','192.168.101.25');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000004','Gateway # 4','192.168.101.26');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000005','Gateway # 5','192.168.101.27');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000006','Gateway # 6','192.168.101.28');
insert into gateway(serial_number,gateway_name,ip_address_v4) values ('GW-000007','Gateway # 7','192.168.101.29');

insert into peripheral(created_at, gateway_id , status , uid , vendor) values ('2021-06-08',1,0,985,'CISCO');

