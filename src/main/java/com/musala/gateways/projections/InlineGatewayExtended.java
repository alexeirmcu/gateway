package com.musala.gateways.projections;

import com.musala.gateways.model.Gateway;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "inlineGatewayExtended", types = {Gateway.class})
public interface InlineGatewayExtended extends  InlineGateway{
    List<InlinePeripheral> getPeripheralList();
}
