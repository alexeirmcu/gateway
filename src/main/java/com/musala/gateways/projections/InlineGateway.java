package com.musala.gateways.projections;

import com.musala.gateways.model.Gateway;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlineGateway", types = {Gateway.class})
public interface InlineGateway {
    Integer getId();
    String getSerialNumber();
    String getGatewayName();
    String getIpAddressV4();
}
