package com.musala.gateways.projections;

import com.musala.gateways.model.EStatus;
import com.musala.gateways.model.Peripheral;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "inlineGateway", types = {Peripheral.class})
public interface InlinePeripheral {
    Integer getId();
    Long getUid();
    String getVendor();
    Date getCreatedAt();
    EStatus getStatus();
    Integer getGatewayId();

}
