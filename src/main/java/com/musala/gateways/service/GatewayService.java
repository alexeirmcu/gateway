package com.musala.gateways.service;

import com.musala.gateways.exceptions.ProcessException;
import com.musala.gateways.exceptions.ValidationException;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.repository.GatewayRepository;
import com.musala.gateways.util.MessageLabelUtil;
import com.musala.gateways.wrappers.GatewayFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Service
public class GatewayService {

    @Autowired
    private GatewayRepository gatewayRepository;

    @Autowired
    private MessageSource messageSource;

    public Page<Gateway> filter(GatewayFilter filters, Pageable pageable){
        int offset = Math.toIntExact(pageable.getOffset());
        int limit = pageable.getPageSize();

        return gatewayRepository.filter(filters, limit, offset, pageable.getSort());
    }

    public Optional<Gateway> findFirstById(Integer id){
        return gatewayRepository.findById(id);
    }

    public Gateway save(Gateway gateway) throws ProcessException {
        if (gateway.getSerialNumber() != null && !isSerialNumberUnique(gateway.getSerialNumber(), gateway.getId())){
            throw new ValidationException("serialNumber",
                MessageLabelUtil.getLabel(messageSource, "duplicateEntry", new Object[] {gateway.getSerialNumber()}, Locale.getDefault()));
        }

        try {
            Gateway result = gatewayRepository.save(gateway);
            return result;
        }catch (Exception e){
            throw new ProcessException(e.getMessage());
        }
    }

    private Boolean isSerialNumberUnique(String serialNumber, Integer id){
        Optional<Gateway> existOptional = gatewayRepository.findFirstBySerialNumber(serialNumber);
        return !existOptional.isPresent() || (id != null && existOptional.get().getId().equals(id));
    }
}
