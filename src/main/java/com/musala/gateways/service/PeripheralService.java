package com.musala.gateways.service;

import com.musala.gateways.exceptions.ProcessException;
import com.musala.gateways.exceptions.ValidationException;
import com.musala.gateways.model.Peripheral;
import com.musala.gateways.repository.PeripheralRepository;
import com.musala.gateways.util.MessageLabelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Service
public class PeripheralService {
    @Autowired
    private PeripheralRepository peripheralRepository;

    @Autowired
    private MessageSource messageSource;

    public Optional<Peripheral> findFirstById(Integer id){
        return peripheralRepository.findById(id);
    }

    public void delete(Integer id) throws ProcessException{
        try {
            peripheralRepository.deleteById(id);
        }catch (Exception e){
            throw  new ProcessException(e.getMessage());
        }
    }

    public Peripheral save(Peripheral peripheral) throws ProcessException {
        if (peripheral.getUid() != null && !isUidUnique(peripheral.getUid(), peripheral.getId())){
            throw new ValidationException("uid", MessageLabelUtil.getLabel(messageSource, "duplicateEntry", new Object[] {peripheral.getUid()}, Locale.getDefault()));
        }

        try {
            return peripheralRepository.save(peripheral);
        }catch (Exception e){
            throw  new ProcessException(e.getMessage());
        }
    }


    private Boolean isUidUnique(long uid, Integer id){
        Optional<Peripheral> existOptional = peripheralRepository.findFirstByUid(uid);
        return !existOptional.isPresent() || (id != null && existOptional.get().getId().equals(id));
    }

}
