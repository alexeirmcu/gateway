package com.musala.gateways.exceptions;


import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class ValidationException extends ProcessException {
    private Map<String, String> errors = new HashMap<>();


    public ValidationException(String key, String message) {
        super(String.format("%s - %s",key, message));
        errors.putIfAbsent(key, message);

    }

    public Map<String, String> getErrors() {
        return errors;
    }

}
