package com.musala.gateways.exceptions;


import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class ProcessException extends RuntimeException {

    public ProcessException(String message) {
        super(message);
    }

    public ProcessException(String message, Throwable cause) {
        super(message, cause);
    }

}
