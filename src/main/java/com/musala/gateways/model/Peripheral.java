package com.musala.gateways.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
public class Peripheral implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Required")
    private Long uid;

    @NotNull(message = "Required")
    private String vendor;

    @Column(name = "gateway_id")
    private Integer gatewayId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @NotNull(message = "Required")
    @Enumerated(EnumType.ORDINAL)
    private EStatus status;

    @ManyToOne
    @JoinColumn(name = "gateway_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Gateway gateway;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public EStatus getStatus() {
        return status;
    }

    public void setStatus(EStatus status) {
        this.status = status;
    }

    public Integer getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Integer gatewayId) {
        this.gatewayId = gatewayId;
    }

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peripheral that = (Peripheral) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(uid, that.uid) &&
                Objects.equals(vendor, that.vendor) &&
                Objects.equals(gatewayId, that.gatewayId) &&
                Objects.equals(createdAt, that.createdAt) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uid, vendor, gatewayId, createdAt, status);
    }

    @Override
    public String toString() {
        return "Peripheral{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", vendor='" + vendor + '\'' +
                ", gatewayId=" + gatewayId +
                ", createdAt=" + createdAt +
                ", status=" + status +
                '}';
    }
}
