package com.musala.gateways.model;

public enum EResponseCode {
    Failure, OK
}
