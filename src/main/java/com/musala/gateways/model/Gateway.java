package com.musala.gateways.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.musala.gateways.validators.IpAddressV4;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
public class Gateway implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "serial_number")
    @NotBlank(message = "Required")
    private String serialNumber;

    @Column(name = "gateway_name")
    @NotBlank(message = "Required")
    private String gatewayName;

    @Column(name = "ip_address_v4")
    @IpAddressV4
    @NotBlank(message = "Required")
    private String ipAddressV4;

    @OneToMany(mappedBy = "gateway")
    @JsonBackReference(value = "peripherical")
    private List<Peripheral> peripheralList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getIpAddressV4() {
        return ipAddressV4;
    }

    public void setIpAddressV4(String ipAddressV4) {
        this.ipAddressV4 = ipAddressV4;
    }

    public List<Peripheral> getPeripheralList() {
        return peripheralList;
    }

    public void setPeripheralList(List<Peripheral> peripheralList) {
        this.peripheralList = peripheralList;
    }


    @Override
    public String toString() {
        return "Gateway{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", gatewayName='" + gatewayName + '\'' +
                ", ipAddressV4='" + ipAddressV4 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gateway gateway = (Gateway) o;
        return Objects.equals(id, gateway.id) &&
                Objects.equals(serialNumber, gateway.serialNumber) &&
                Objects.equals(gatewayName, gateway.gatewayName) &&
                Objects.equals(ipAddressV4, gateway.ipAddressV4);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serialNumber, gatewayName, ipAddressV4);
    }
}
