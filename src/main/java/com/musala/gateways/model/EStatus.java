package com.musala.gateways.model;

public enum EStatus {
    ONLINE, OFFLINE
}
