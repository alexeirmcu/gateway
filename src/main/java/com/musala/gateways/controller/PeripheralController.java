package com.musala.gateways.controller;

import com.musala.gateways.exceptions.ProcessException;
import com.musala.gateways.exceptions.ValidationException;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.Peripheral;
import com.musala.gateways.service.GatewayService;
import com.musala.gateways.service.PeripheralService;
import com.musala.gateways.util.MessageLabelUtil;
import com.musala.gateways.wrappers.ResponseWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.*;

@RestController
@RequestMapping(value = "/peripheral")
public class PeripheralController {

    private final Logger log = LoggerFactory.getLogger(PeripheralController.class);

    public final static int MAX_PERIPHERAL_PER_GW = 10;

    @Autowired
    GatewayService gatewayService;

    @Autowired
    PeripheralService peripheralService;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> findById(
            @PathVariable int id,
            HttpServletRequest request
    )throws URISyntaxException {

        Optional<Peripheral> peripheralOpt = peripheralService.findFirstById(id);

        if (!peripheralOpt.isPresent()){
            return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(peripheralOpt.get(), null, HttpStatus.OK);
    }


    @RequestMapping(value = "/{gatewayId}/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> create(
            HttpServletRequest httpServletRequest,
            @PathVariable int gatewayId,
            @RequestBody @Valid Peripheral peripheral) throws URISyntaxException {
        log.debug("REST request to addPeripheral: {}, to gateway id: {}", peripheral, gatewayId);
        ResponseWrapper responseWrapper = new ResponseWrapper();
        try {
            Optional<Gateway> gatewayOptional = gatewayService.findFirstById(gatewayId);

            if (!gatewayOptional.isPresent()){
                throw new ValidationException("gatewayId",
                        MessageLabelUtil.getLabel(messageSource, "invalidParameterReceived", new Object[] {"gateway", gatewayId}, Locale.getDefault()));
            }

            Gateway gateway = gatewayOptional.get();

            if (gateway.getPeripheralList().size() >= MAX_PERIPHERAL_PER_GW){
                throw new ValidationException("gatewayId", MessageLabelUtil.getLabel(messageSource, "gatewayMaxCapacityReached",new Object[] {gateway.getSerialNumber()}, Locale.getDefault()));
            }

            peripheral.setGatewayId(gatewayId);
            peripheral.setCreatedAt(new Date(System.currentTimeMillis()));

            Peripheral saved = peripheralService.save(peripheral);
            responseWrapper.setDetailedInfo(saved);
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.CREATED);

        }catch (ValidationException ve){
            responseWrapper.setMessage(MessageLabelUtil.getLabel(messageSource, "errorOcurred", Locale.getDefault()));
            responseWrapper.setDetailedInfo(ve.getErrors());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            responseWrapper.setMessage(e.getMessage());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/{gatewayId}/update/{peripheralId}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> update(
            HttpServletRequest httpServletRequest,
            @PathVariable int peripheralId,
            @PathVariable int gatewayId,
            @RequestBody @Valid Peripheral peripheral) throws URISyntaxException {
        log.debug("REST request to update peripheral with id:{}, using data: {}", peripheralId, peripheral);
        ResponseWrapper responseWrapper = new ResponseWrapper();
        try {
            Optional<Gateway> gatewayOptional = gatewayService.findFirstById(gatewayId);

            if (!gatewayOptional.isPresent()){
                throw new ValidationException("gatewayId",
                    MessageLabelUtil.getLabel(messageSource, "invalidParameterReceived",
                            new Object[] {"gateway", gatewayId}, Locale.getDefault())
                );
            }

            Optional<Peripheral> peripheralOptional = peripheralService.findFirstById(peripheralId);

            if (!peripheralOptional.isPresent()){
                throw new ValidationException("peripheralId",
                        MessageLabelUtil.getLabel(messageSource, "invalidParameterReceived",
                        new Object[] {"peripheral", peripheralId}, Locale.getDefault()));
            }

            peripheral.setGatewayId(gatewayId);
            peripheral.setId(peripheralId);

            Peripheral saved = peripheralService.save(peripheral);
            responseWrapper.setDetailedInfo(saved);
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.OK);

        }catch (ValidationException ve){
            responseWrapper.setMessage(MessageLabelUtil.getLabel(messageSource, "errorOcurred", Locale.getDefault()));
            responseWrapper.setDetailedInfo(ve.getErrors());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            responseWrapper.setMessage(e.getMessage());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{peripheralId}/delete",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> delete(
            HttpServletRequest httpServletRequest,
            @PathVariable int peripheralId
            ) throws URISyntaxException {
        log.debug("REST request to deletePeripheral: {},",peripheralId);

        ResponseWrapper responseWrapper = new ResponseWrapper();
        try{
            peripheralService.delete(peripheralId);
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.OK);
        }catch (ValidationException ve){
            responseWrapper.setMessage(MessageLabelUtil.getLabel(messageSource, "errorOcurred", Locale.getDefault()));
            responseWrapper.setDetailedInfo(ve.getErrors());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            responseWrapper.setMessage(e.getMessage());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseWrapper handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        ResponseWrapper responseWrapper = new ResponseWrapper("Some error ocurred", errors);

        return responseWrapper;
    }
}
