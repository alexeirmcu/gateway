package com.musala.gateways.controller;

import com.musala.gateways.exceptions.ProcessException;
import com.musala.gateways.exceptions.ValidationException;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.projections.InlineGateway;
import com.musala.gateways.projections.InlineGatewayExtended;
import com.musala.gateways.service.GatewayService;
import com.musala.gateways.service.PeripheralService;
import com.musala.gateways.util.MessageLabelUtil;
import com.musala.gateways.util.PaginationUtil;
import com.musala.gateways.wrappers.GatewayFilter;
import com.musala.gateways.wrappers.ResponseWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.*;

@RestController
@RequestMapping(value = "/gateway")
public class GatewayController {

    private final Logger log = LoggerFactory.getLogger(GatewayController.class);

    @Autowired
    private GatewayService gatewayService;

    @Autowired
    private ProjectionFactory projectionFactory;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/filter",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> getLabCase(
            @RequestParam(value = "serialNumber", required = false) String serialNumber,
            @RequestParam(value = "gatewayName", required = false) String gatewayName,
            @RequestParam(value = "containPeriphericalWithVendor", required = false) String containPeriphericalWithVendor,
            HttpServletRequest request,
            Pageable pageable
    )throws URISyntaxException {
        GatewayFilter filters = new GatewayFilter();
        filters.setName(gatewayName);
        filters.setSerialNumber(serialNumber);
        filters.setContainPeriphericalWithVendor(containPeriphericalWithVendor);

        Page<Gateway> page = gatewayService.filter(filters, pageable);

        Page<InlineGateway> projected = page.map(l -> projectionFactory.createProjection(InlineGateway.class, l));

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/gateway/filter", Math.toIntExact(pageable.getOffset()), pageable.getPageSize());

        return new ResponseEntity<>(projected, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> findById(
            @PathVariable int id,
            HttpServletRequest request
    )throws URISyntaxException {

        Optional<Gateway> gatewayOpt = gatewayService.findFirstById(id);

        if (!gatewayOpt.isPresent()){
            return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
        }

        InlineGatewayExtended projected = projectionFactory.createProjection(InlineGatewayExtended.class, gatewayOpt.get());

        return new ResponseEntity<>(projected, null, HttpStatus.OK);
    }


    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> create(
            HttpServletRequest httpServletRequest,
            @RequestBody @Valid Gateway gateway) throws URISyntaxException{
        log.debug("REST request to create gateway : {}", gateway);

        ResponseWrapper responseWrapper = new ResponseWrapper();
        try {
            Gateway saved = gatewayService.save(gateway);
            responseWrapper.setDetailedInfo(saved);
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.CREATED);

        }catch (ValidationException ve){
            responseWrapper.setMessage(MessageLabelUtil.getLabel(messageSource, "errorOcurred", Locale.getDefault()));
            responseWrapper.setDetailedInfo(ve.getErrors());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            responseWrapper.setMessage(e.getMessage());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}/update",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> update(
            HttpServletRequest httpServletRequest,
            @PathVariable int id,
            @RequestBody @Valid Gateway gateway) throws URISyntaxException{
        log.debug("REST request to update gateway : {}", gateway);
        ResponseWrapper responseWrapper = new ResponseWrapper();
        try {

            Optional<Gateway> gatewayOptional = gatewayService.findFirstById(id);

            if (!gatewayOptional.isPresent()){
                throw new ValidationException("gatewayId",
                        MessageLabelUtil.getLabel(messageSource, "invalidParameterReceived",
                                new Object[] {"gateway", id}, Locale.getDefault())
                );
            }

            gateway.setId(id);
            Gateway saved = gatewayService.save(gateway);
            responseWrapper.setDetailedInfo(saved);
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.OK);
        }catch (ValidationException ve){
            responseWrapper.setMessage(MessageLabelUtil.getLabel(messageSource, "errorOcurred", Locale.getDefault()));
            responseWrapper.setDetailedInfo(ve.getErrors());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }catch (Exception e){
            responseWrapper.setMessage(e.getMessage());
            return new ResponseEntity<>(responseWrapper, null, HttpStatus.BAD_REQUEST);
        }

    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseWrapper handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        ResponseWrapper responseWrapper = new ResponseWrapper(
                MessageLabelUtil.getLabel(messageSource, "errorOcurred",null, Locale.getDefault()), errors);

        return responseWrapper;
    }

}
