package com.musala.gateways.wrappers;

public class GatewayFilter {
    private String serialNumber;
    private String name;
    private String containPeriphericalWithVendor;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContainPeriphericalWithVendor() {
        return containPeriphericalWithVendor;
    }

    public void setContainPeriphericalWithVendor(String containPeriphericalWithVendor) {
        this.containPeriphericalWithVendor = containPeriphericalWithVendor;
    }


}
