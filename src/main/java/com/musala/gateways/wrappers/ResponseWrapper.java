package com.musala.gateways.wrappers;

import com.musala.gateways.model.EResponseCode;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ResponseWrapper implements Serializable {
    public static final String DEFAULT_SUCCESS_MSG = "Success";

    String message;
    Object detailedInfo;

    public Object getDetailedInfo() {
        return detailedInfo;
    }

    public void setDetailedInfo(Object detailedInfo) {
        this.detailedInfo = detailedInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseWrapper() {
        message = DEFAULT_SUCCESS_MSG;
    }

    public ResponseWrapper(String message, Object detailedInfo) {

        this.message = message;
        this.detailedInfo = detailedInfo;
    }

}
