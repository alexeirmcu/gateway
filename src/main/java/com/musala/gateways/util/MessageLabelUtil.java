package com.musala.gateways.util;

import org.springframework.context.MessageSource;
import org.springframework.lang.Nullable;

import java.util.Locale;

public class MessageLabelUtil {
    public static String getLabel(MessageSource messageSource, String key, Locale locale){
        try {
            return messageSource.getMessage(key,null, locale);
        }catch (Exception e){
            return key;
        }
    }
    public static String getLabel(MessageSource messageSource, String key, @Nullable Object[] data, Locale locale){
        try {
            return messageSource.getMessage(key,data, locale);
        }catch (Exception e){
            return key;
        }
    }
}
