package com.musala.gateways.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class PaginationUtil {

    public static final int DEFAULT_OFFSET = 0;

    public static final int MIN_OFFSET = 0;

    public static final int DEFAULT_LIMIT = 20;

    public static final int MAX_LIMIT = 10000;

    public static LinkedList<Order> extractOrders (CriteriaBuilder builder, Root<?> entity,Sort sort){
        LinkedList<Order> list = new LinkedList<>();
        if(sort != null){
            Iterator<Sort.Order> it = sort.iterator();
            while (it.hasNext()){
                Order order = null;
                Sort.Order sortOrder = it.next();
                if(sortOrder.getDirection().isAscending()){
                    order = builder.asc(entity.get(sortOrder.getProperty()));
                }
                else{
                    order = builder.desc(entity.get(sortOrder.getProperty()));
                }
                list.add(order);
            }
        }
        return list;
    }

    public static Pageable generatePageRequest(Integer offset, Integer limit) {
        if (offset == null || offset < MIN_OFFSET) {
            offset = DEFAULT_OFFSET;
        }
        if (limit == null || limit > MAX_LIMIT) {
            limit = DEFAULT_LIMIT;
        }

        Integer pageNumber = offset / limit;
        PageRequest pageRequest = PageRequest.of(pageNumber, limit);
        return pageRequest;
    }

    public static HttpHeaders generatePaginationHttpHeaders(Page<?> page, String baseUrl, Integer offset, Integer limit)
            throws URISyntaxException {

        if (offset == null || offset < MIN_OFFSET) {
            offset = DEFAULT_OFFSET;
        }
        if (limit == null || limit > MAX_LIMIT) {
            limit = DEFAULT_LIMIT;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", "" + page.getTotalElements());
        String link = "";
        if (offset < page.getTotalPages()) {
            link = "<" + (new URI(baseUrl +"?page=" + (offset + 1) + "&limit=" + limit)).toString()
                    + ">; rel=\"next\",";
        }
        if (offset > 0) {//@fixed changed 1 to 0
            link += "<" + (new URI(baseUrl +"?page=" + (offset - 1) + "&limit=" + limit)).toString()
                    + ">; rel=\"prev\",";
        }
        link += "<" + (new URI(baseUrl +"?page=" + page.getTotalPages() + "&limit=" + limit)).toString()
                + ">; rel=\"last\"," +
                "<" + (new URI(baseUrl +"?page=" + 0 + "&limit=" + limit)).toString()
                + ">; rel=\"first\"";
        headers.add(HttpHeaders.LINK, link);
        return headers;
    }

    public static long count(EntityManager entityManager, CriteriaBuilder criteriaBuilder, Class<?> entityClass, Predicate[] predArray) {
        CriteriaQuery<Long> cq = criteriaBuilder.createQuery(Long.class);
        cq.select(criteriaBuilder.count(cq.from(entityClass)));
        cq.where(predArray);
        return entityManager.createQuery(cq).getSingleResult();
    }

}
