package com.musala.gateways.repository;

import com.musala.gateways.model.Gateway;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


public interface GatewayRepository extends PagingAndSortingRepository<Gateway, Integer>, GatewayRepositoryCustom{
    List<Gateway> findAllBySerialNumberIsLike(String serialNumber);
    Optional<Gateway> findFirstBySerialNumber(String serialNumber);
}
