package com.musala.gateways.repository;

import com.musala.gateways.model.Peripheral;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PeripheralRepository extends CrudRepository<Peripheral, Integer> {
    long countByGatewayId(Integer gatewayId);
    List<Peripheral> findAllByGatewayId(Integer gatewayId);
    Optional<Peripheral> findFirstByUid(long uid);
}
