package com.musala.gateways.repository;

import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.Peripheral;
import com.musala.gateways.util.PaginationUtil;
import com.musala.gateways.wrappers.GatewayFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;

public class GatewayRepositoryImpl implements GatewayRepositoryCustom {
    private final EntityManager em;

    public GatewayRepositoryImpl(EntityManager entityManager) {
        this.em = entityManager;
    }

    @Override
    public Page<Gateway> filter(GatewayFilter filters, int limit, int skip, Sort sort) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Gateway> query = builder.createQuery(Gateway.class);

        Root<Gateway> root = query.from(Gateway.class);

        List<Predicate> predList = buildPredicates(builder, query, root, filters);

        Predicate[] predArray = new Predicate[predList.size()];
        predList.toArray(predArray);

        query.where(predArray).orderBy(PaginationUtil.extractOrders(builder, root, sort));

        Query runnableQuery = em.createQuery(query).setFirstResult(skip).setMaxResults(limit);

        List<Gateway> list = runnableQuery.getResultList();
        Pageable pageable = PaginationUtil.generatePageRequest(skip, limit);
        long totalCount = PaginationUtil.count(em, builder, Gateway.class,predArray);
        Page<Gateway> page = new PageImpl<>(list, pageable, totalCount);
        return page;

    }

    @Override
    public List<Predicate> buildPredicates(CriteriaBuilder builder, CriteriaQuery query, Root<Gateway> object, GatewayFilter filters){
        List<Predicate> predList = new LinkedList<>();

        if (filters.getName() != null){
            predList.add(
                builder.and(
                    builder.like(builder.lower(object.get("gatewayName")), "%".concat(filters.getName()).toLowerCase().concat("%"))
                )
            );
        }

        if (filters.getSerialNumber() != null){
            predList.add(
                builder.and(
                    builder.like(builder.lower(object.get("serialNumber")), "%".concat(filters.getSerialNumber()).toLowerCase().concat("%"))
                )
            );
        }

        if (filters.getContainPeriphericalWithVendor() != null){
            Join<Gateway, Peripheral> periphericalJoin = object.join("periphericalList", JoinType.LEFT);

            predList.add(
                builder.like(builder.lower(periphericalJoin.get("vendor")), "%".concat(filters.getContainPeriphericalWithVendor()).toLowerCase().concat("%"))
            );
        }

        return  predList;
    }
}
