package com.musala.gateways.repository;

import com.musala.gateways.model.Gateway;
import com.musala.gateways.wrappers.GatewayFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import javax.persistence.criteria.*;
import java.util.List;

public interface GatewayRepositoryCustom {
    Page<Gateway> filter(GatewayFilter filters, int limit, int skip, Sort sort);

    List<Predicate> buildPredicates(CriteriaBuilder builder, CriteriaQuery query, Root<Gateway> object, GatewayFilter filters);


}
