package com.musala.gateways.validators;

import com.musala.gateways.repository.GatewayRepository;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


public class UniqueSerialNumberValidator implements ConstraintValidator<UniqueSerialNumber, String> {

    private GatewayRepository gatewayRepository;

    public UniqueSerialNumberValidator(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    public void initialize(UniqueSerialNumber constraint) {
    }

    public boolean isValid(String serialNumber, ConstraintValidatorContext context) {
        return serialNumber != null && !gatewayRepository.findFirstBySerialNumber(serialNumber).isPresent();
    }

}
