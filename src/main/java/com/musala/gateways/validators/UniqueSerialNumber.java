package com.musala.gateways.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueSerialNumberValidator.class)
public @interface UniqueSerialNumber {
    String message() default "{com.musala.gateways.UniqueSerialNumber.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}