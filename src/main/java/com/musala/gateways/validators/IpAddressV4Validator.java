package com.musala.gateways.validators;

import com.musala.gateways.repository.GatewayRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class IpAddressV4Validator implements ConstraintValidator<IpAddressV4, String> {

    private static final String IPV4_PATTERN =
            "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.(?!$)|$)){4}$";

    private static final Pattern pattern = Pattern.compile(IPV4_PATTERN);

    public void initialize(IpAddressV4 constraint) {
    }

    public boolean isValid(String ipAddress, ConstraintValidatorContext context) {
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }

}
