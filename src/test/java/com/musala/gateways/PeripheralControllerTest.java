package com.musala.gateways;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.musala.gateways.model.EStatus;
import com.musala.gateways.model.Peripheral;
import com.musala.gateways.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.musala.gateways.controller.PeripheralController;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.service.GatewayService;
import com.musala.gateways.service.PeripheralService;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@WebMvcTest(PeripheralController.class)
public class PeripheralControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PeripheralService peripheralService;

    @MockBean
    private GatewayService gatewayService;

    @Test
    public void givenPeripheralId_thenReturnPeripheral() throws Exception {
        int gatewayId = 1;
        int peripheralId = 1;

        Peripheral pr1 = new Peripheral();
        pr1.setGatewayId(gatewayId);
        pr1.setUid(1234L);
        pr1.setCreatedAt(new Date(System.currentTimeMillis()));
        pr1.setId(peripheralId);
        pr1.setStatus(EStatus.ONLINE);
        pr1.setVendor("HUAWEI");

        Optional<Peripheral> opPr1 = Optional.of(pr1);

        given(peripheralService.findFirstById(peripheralId)).willReturn(opPr1);

        mvc.perform(get("/peripheral/"+peripheralId).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(pr1.getId())));

    }

    @Test
    public void whenPostPeripheral_thenAddToGateway() throws Exception {
        int gatewayId = 1;
        int wrongGatewayId = Integer.MAX_VALUE;
        Gateway gtw1 = new Gateway();
        gtw1.setId(gatewayId);

        gtw1.setPeripheralList(new ArrayList<>());

        Optional<Gateway> opGwt1 = Optional.of(gtw1);

        int peripheralId = 1;
        Peripheral pr1 = new Peripheral();
        pr1.setGatewayId(gatewayId);
        pr1.setUid(1234L);
        pr1.setCreatedAt(new Date(System.currentTimeMillis()));
        pr1.setId(peripheralId);
        pr1.setStatus(EStatus.ONLINE);
        pr1.setVendor("HUAWEI");

        given(gatewayService.findFirstById(gatewayId)).willReturn(opGwt1);

        given(peripheralService.save(Mockito.any())).willReturn(pr1);

        mvc.perform(post(String.format("/peripheral/%s/create",gatewayId)).contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(pr1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.detailedInfo.id", is(pr1.getId())));
        ;

        mvc.perform(post(String.format("/peripheral/%s/create",wrongGatewayId)).contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(pr1)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("errorOcurred")))
                .andExpect(jsonPath("$.detailedInfo.gatewayId", is("invalidParameterReceived")))
        ;
    }

    @Test
    public void whenPostPeripheral_thenValidateMaxCapacityGateway() throws Exception {
        int gatewayId = 1;
        Gateway gtw1 = new Gateway();
        gtw1.setId(gatewayId);

        gtw1.setPeripheralList(new ArrayList<>());

        for (int i = 0; i < PeripheralController.MAX_PERIPHERAL_PER_GW; i++) {
            gtw1.getPeripheralList().add(new Peripheral());
        }

        Optional<Gateway> opGwt1 = Optional.of(gtw1);

        int peripheralId = 1;
        Peripheral pr1 = new Peripheral();
        pr1.setGatewayId(gatewayId);
        pr1.setUid(1234L);
        pr1.setCreatedAt(new Date(System.currentTimeMillis()));
        pr1.setId(peripheralId);
        pr1.setStatus(EStatus.ONLINE);
        pr1.setVendor("HUAWEI");

        given(gatewayService.findFirstById(gatewayId)).willReturn(opGwt1);

        given(peripheralService.save(Mockito.any())).willReturn(pr1);

        mvc.perform(post(String.format("/peripheral/%s/create",gatewayId)).contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(pr1)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("errorOcurred")))
                .andExpect(jsonPath("$.detailedInfo.gatewayId", is("gatewayMaxCapacityReached")))
                ;
    }


}
