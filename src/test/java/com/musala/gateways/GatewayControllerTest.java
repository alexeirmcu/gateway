package com.musala.gateways;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.musala.gateways.projections.InlineGatewayExtended;
import com.musala.gateways.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.musala.gateways.controller.GatewayController;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.service.GatewayService;


import java.util.Optional;


@WebMvcTest(GatewayController.class)
public class GatewayControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GatewayService gatewayService;

    @MockBean
    private ProjectionFactory projectionFactory ;

    @Test
    public void whenPostGateway_thenCreateGateway() throws Exception {
        Gateway gtw8 = new Gateway();
        gtw8.setId(8);
        gtw8.setSerialNumber("GW-000007");
        gtw8.setIpAddressV4("192.168.101.56");
        gtw8.setGatewayName("Gateway # 8");

        given(gatewayService.save(Mockito.any())).willReturn(gtw8);

        mvc.perform(post("/gateway/create").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(gtw8)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.detailedInfo.id", is(gtw8.getId())));

        verify(gatewayService, VerificationModeFactory.times(1)).save(Mockito.any());
        reset(gatewayService);
    }


}
