# Gateway 

deploy steps.

1. To install project open console app and run.
 `mvn clean install`

2. Navigate to target directory and run

 `java -jar gateways-0.0.1-SNAPSHOT.jar`
 
3. To test project import postman collection.

4. To run unitary test
 `mvn test` 

